<h3>Установка</h3>

1. Клонируйте проект посредством ПО(например: git cli, Visual Studio, Rider, Github Desktop)
2. В Unigine SDK Browser нажать Add Existing, после нажать на кнопку Browse и выбрать папку BigMamaProject.

<h3>Setup</h3>

1. Clone project with special software(for example: git cli, Visual Studio, Rider, Github Desktop)
2. In Unigine SDK Browser click at ``Add Existing, after click at Browse``` and select folder BigMamaProject.