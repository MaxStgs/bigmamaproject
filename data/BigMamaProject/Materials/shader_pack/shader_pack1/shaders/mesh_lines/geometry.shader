/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


#include <core/shaders/default/common/geometry_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

layout(triangles) in;
layout(triangle_strip,max_vertices = 4) out;

in float4 s_geom_texcoord_0[];
in float4 s_geom_texcoord_1[];

/*
 */
void main() {
	
	float4 position_0 = s_geom_texcoord_0[0];
	float4 position_1 = gl_in[0].gl_Position;
	float4 position_2 = gl_in[1].gl_Position;
	float4 position_3 = gl_in[2].gl_Position;
	
	float4 color_1 = s_geom_texcoord_1[0];
	float4 color_2 = s_geom_texcoord_1[1];
	
	float size_1 = s_geom_texcoord_0[0].w;
	float size_2 = s_geom_texcoord_0[1].w;
	
	float radius_1 = max(getPosition(position_1).w * s_viewport.w / s_projection[1].y,0.0f) * size_1;
	float radius_2 = max(getPosition(position_2).w * s_viewport.w / s_projection[1].y,0.0f) * size_2;
	
	float2 direction_10 = normalize(position_1.xy - position_0.xy);
	float2 direction_21 = normalize(position_2.xy - position_1.xy);
	float2 direction_32 = normalize(position_3.xy - position_2.xy);
	
	float2 tangent_1 = normalize(direction_10 + direction_21);
	float2 tangent_2 = normalize(direction_21 + direction_32);
	
	tangent_1 = float2(-tangent_1.y,tangent_1.x) * radius_1;
	tangent_2 = float2(-tangent_2.y,tangent_2.x) * radius_2;
	
	gl_Position = getPosition(position_1 + float4(tangent_1,0.0f,0.0f));
	s_texcoord_0 = color_1;
	EmitVertex();
	
	gl_Position = getPosition(position_1 - float4(tangent_1,0.0f,0.0f));
	s_texcoord_0 = color_1;
	EmitVertex();
	
	gl_Position = getPosition(position_2 + float4(tangent_2,0.0f,0.0f));
	s_texcoord_0 = color_2;
	EmitVertex();
	
	gl_Position = getPosition(position_2 - float4(tangent_2,0.0f,0.0f));
	s_texcoord_0 = color_2;
	EmitVertex();
	
	EndPrimitive();
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

struct GEOMETRY_IN {
	float4 position : SV_POSITION;
	float4 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
};

struct GEOMETRY_OUT {
	float4 position : SV_POSITION;
	float4 texcoord_0 : TEXCOORD0;
};

/*
 */
[maxvertexcount(4)]
void main(triangle GEOMETRY_IN IN[3],inout TriangleStream<GEOMETRY_OUT> stream) {
	
	GEOMETRY_OUT OUT;
	
	float4 position_0 = IN[0].texcoord_0;
	float4 position_1 = IN[0].position;
	float4 position_2 = IN[1].position;
	float4 position_3 = IN[2].position;
	
	float4 color_1 = IN[0].texcoord_1;
	float4 color_2 = IN[1].texcoord_1;
	
	float size_1 = IN[0].texcoord_0.w;
	float size_2 = IN[1].texcoord_0.w;
	
	float radius_1 = max(getPosition(position_1).w * s_viewport.w / s_projection[1].y,0.0f) * size_1;
	float radius_2 = max(getPosition(position_2).w * s_viewport.w / s_projection[1].y,0.0f) * size_2;
	
	float2 direction_10 = normalize(position_1.xy - position_0.xy);
	float2 direction_21 = normalize(position_2.xy - position_1.xy);
	float2 direction_32 = normalize(position_3.xy - position_2.xy);
	
	float2 tangent_1 = normalize(direction_10 + direction_21);
	float2 tangent_2 = normalize(direction_21 + direction_32);
	
	tangent_1 = float2(-tangent_1.y,tangent_1.x) * radius_1;
	tangent_2 = float2(-tangent_2.y,tangent_2.x) * radius_2;
	
	OUT.position = getPosition(position_1 + float4(tangent_1,0.0f,0.0f));
	OUT.texcoord_0 = color_1;
	stream.Append(OUT);
	
	OUT.position = getPosition(position_1 - float4(tangent_1,0.0f,0.0f));
	OUT.texcoord_0 = color_1;
	stream.Append(OUT);
	
	OUT.position = getPosition(position_2 + float4(tangent_2,0.0f,0.0f));
	OUT.texcoord_0 = color_2;
	stream.Append(OUT);
	
	OUT.position = getPosition(position_2 - float4(tangent_2,0.0f,0.0f));
	OUT.texcoord_0 = color_2;
	stream.Append(OUT);
	
	stream.RestartStrip();
}

#endif
