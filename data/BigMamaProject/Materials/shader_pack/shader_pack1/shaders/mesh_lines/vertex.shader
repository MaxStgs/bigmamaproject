/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


#include <core/shaders/default/common/vertex_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

in float4 s_attribute_0;
in float4 s_attribute_1;
in float4 s_attribute_2;
in float4 s_attribute_3;

out float4 s_geom_texcoord_0;
out float4 s_geom_texcoord_1;

/*
 */
void main() {
	
	float4 row_0 = s_transform[0];
	float4 row_1 = s_transform[1];
	float4 row_2 = s_transform[2];
	
	float4 position = s_attribute_0;
	gl_Position = mul4(row_0,row_1,row_2,position);
	
	float4 previous = float4(s_attribute_2.xyz,1.0f);
	s_geom_texcoord_0 = float4(mul4(row_0,row_1,row_2,previous).xyz,s_attribute_2.w);
	
	s_geom_texcoord_1 = s_attribute_3;
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

struct VERTEX_IN {
	float4 position : POSITION;
	float4 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
	float4 texcoord_2 : TEXCOORD2;
	uint instance : SV_INSTANCEID;
};

struct VERTEX_OUT {
	float4 position : SV_POSITION;
	float4 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
};

/*
 */
VERTEX_OUT main(VERTEX_IN IN) {
	
	VERTEX_OUT OUT;
	
	float4 row_0 = s_transform[0];
	float4 row_1 = s_transform[1];
	float4 row_2 = s_transform[2];
	
	float4 position = IN.position;
	OUT.position = mul4(row_0,row_1,row_2,position);
	
	float4 previous = float4(IN.texcoord_1.xyz,1.0f);
	OUT.texcoord_0 = float4(mul4(row_0,row_1,row_2,previous).xyz,IN.texcoord_1.w);
	
	OUT.texcoord_1 = IN.texcoord_2;
	
	return OUT;
}

#endif
