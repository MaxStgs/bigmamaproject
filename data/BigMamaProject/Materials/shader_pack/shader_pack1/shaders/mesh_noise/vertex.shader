/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


#include <core/shaders/default/common/vertex_base.h>
#include <core/shaders/default/mesh/vertex_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

in float4 s_attribute_0;
in float4 s_attribute_1;
in float4 s_attribute_2;
in float4 s_attribute_3;

UNIFORM_BUFFER_BEGIN(shader_instance_parameters)
	uniform float4 s_instances[96];
UNIFORM_BUFFER_END

uniform float4 base_transform;

uniform float4 noise_transform;
uniform float4 vertex_noise[66];

/*
 */
float noise(float3 v) {
	
	v = abs(v);
	
	float3 i = fract(v * (1.0f / 32.0f)) * 32.0f;
	float3 f = fract(v);
	
	float2 p;
	p.x = vertex_noise[int(i.x) + 0].w;
	p.y = vertex_noise[int(i.x) + 1].w;
	p += i.y;
	
	float4 b;
	b.x = vertex_noise[int(p.x) + 0].w;
	b.y = vertex_noise[int(p.y) + 0].w;
	b.z = vertex_noise[int(p.x) + 1].w;
	b.w = vertex_noise[int(p.y) + 1].w;
	b += i.z;
	
	float4 r0;
	r0.x = dot(vertex_noise[int(b.x) + 0].xyz,f - float3(0.0f,0.0f,0.0f));
	r0.y = dot(vertex_noise[int(b.y) + 0].xyz,f - float3(1.0f,0.0f,0.0f));
	r0.z = dot(vertex_noise[int(b.z) + 0].xyz,f - float3(0.0f,1.0f,0.0f));
	r0.w = dot(vertex_noise[int(b.w) + 0].xyz,f - float3(1.0f,1.0f,0.0f));
	
	float4 r1;
	r1.x = dot(vertex_noise[int(b.x) + 1].xyz,f - float3(0.0f,0.0f,1.0f));
	r1.y = dot(vertex_noise[int(b.y) + 1].xyz,f - float3(1.0f,0.0f,1.0f));
	r1.z = dot(vertex_noise[int(b.z) + 1].xyz,f - float3(0.0f,1.0f,1.0f));
	r1.w = dot(vertex_noise[int(b.w) + 1].xyz,f - float3(1.0f,1.0f,1.0f));
	
	f = (3.0f - 2.0f * f) * f * f;
	
	r0 = lerp(r0,r1,f.z);
	p = lerp(r0.xy,r0.zw,f.y);
	return lerp(p.x,p.y,f.x);
}

/*
 */
void main() {
	
	float4 row_0,row_1,row_2;
	
	if(gl_InstanceID == 0) {
		row_0 = s_transform[0];
		row_1 = s_transform[1];
		row_2 = s_transform[2];
	} else {
		int3 instance = int3(gl_InstanceID * 3) + int3(0,1,2);
		row_0 = s_instances[instance.x];
		row_1 = s_instances[instance.y];
		row_2 = s_instances[instance.z];
	}
	
	float4 position = float4(s_attribute_0.xyz,1.0f);
	float4 vertex = mul4(row_0,row_1,row_2,position);
	
	float3 tangent,binormal,normal;
	getTangentBasis(s_attribute_2,tangent,binormal,normal);
	
	normal = normalize(mul3(row_0,row_1,row_2,normal));
	tangent = normalize(mul3(row_0,row_1,row_2,tangent));
	binormal = normalize(mul3(row_0,row_1,row_2,binormal));
	
	vertex.xyz += normal * noise(position.xyz * 0.5f + noise_transform.xyz) * 0.500f;
	vertex.xyz += normal * noise(position.xyz * 1.0f + noise_transform.xyz) * 0.250f;
	
	float4 texcoord = s_attribute_1;
	texcoord.xy = texcoord.xy * base_transform.xy + base_transform.zw;
	
	normal *= s_polygon_front;
	
	#include <core/shaders/default/mesh/vertex_base.h>
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

struct VERTEX_IN {
	float4 position : POSITION;
	float4 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
	float4 texcoord_2 : TEXCOORD2;
	uint instance : SV_INSTANCEID;
};

cbuffer shader_instance_parameters {
	float4 s_instances[96];
};

cbuffer shader_parameters {
	float4 base_transform;
};

float4 noise_transform;
float4 vertex_noise[66];

/*
 */
float noise(float3 v) {
	
	v = abs(v);
	
	float3 i = frac(v * (1.0f / 32.0f)) * 32.0f;
	float3 f = frac(v);
	
	float2 p;
	p.x = vertex_noise[(int)i.x + 0].w;
	p.y = vertex_noise[(int)i.x + 1].w;
	p += i.y;
	
	float4 b;
	b.x = vertex_noise[(int)p.x + 0].w;
	b.y = vertex_noise[(int)p.y + 0].w;
	b.z = vertex_noise[(int)p.x + 1].w;
	b.w = vertex_noise[(int)p.y + 1].w;
	b += i.z;
	
	float4 r0;
	r0.x = dot(vertex_noise[(int)b.x + 0].xyz,f - float3(0.0f,0.0f,0.0f));
	r0.y = dot(vertex_noise[(int)b.y + 0].xyz,f - float3(1.0f,0.0f,0.0f));
	r0.z = dot(vertex_noise[(int)b.z + 0].xyz,f - float3(0.0f,1.0f,0.0f));
	r0.w = dot(vertex_noise[(int)b.w + 0].xyz,f - float3(1.0f,1.0f,0.0f));
	
	float4 r1;
	r1.x = dot(vertex_noise[(int)b.x + 1].xyz,f - float3(0.0f,0.0f,1.0f));
	r1.y = dot(vertex_noise[(int)b.y + 1].xyz,f - float3(1.0f,0.0f,1.0f));
	r1.z = dot(vertex_noise[(int)b.z + 1].xyz,f - float3(0.0f,1.0f,1.0f));
	r1.w = dot(vertex_noise[(int)b.w + 1].xyz,f - float3(1.0f,1.0f,1.0f));
	
	f = (3.0f - 2.0f * f) * f * f;
	
	r0 = lerp(r0,r1,f.z);
	p = lerp(r0.xy,r0.zw,f.y);
	return lerp(p.x,p.y,f.x);
}

/*
 */
VERTEX_OUT main(VERTEX_IN IN) {
	
	VERTEX_OUT OUT;
	
	float4 row_0,row_1,row_2;
	
	[flatten] if(IN.instance == 0) {
		row_0 = s_transform[0];
		row_1 = s_transform[1];
		row_2 = s_transform[2];
	} else {
		int3 instance = IN.instance * 3 + int3(0,1,2);
		row_0 = s_instances[instance.x];
		row_1 = s_instances[instance.y];
		row_2 = s_instances[instance.z];
	}
	
	float4 position = float4(IN.position.xyz,1.0f);
	float4 vertex = mul4(row_0,row_1,row_2,position);
	
	float3 tangent,binormal,normal;
	getTangentBasis(IN.texcoord_1,tangent,binormal,normal);
	
	normal = normalize(mul3(row_0,row_1,row_2,normal));
	tangent = normalize(mul3(row_0,row_1,row_2,tangent));
	binormal = normalize(mul3(row_0,row_1,row_2,binormal));
	
	vertex.xyz += normal * noise(position.xyz * 0.5f + noise_transform.xyz) * 0.500f;
	vertex.xyz += normal * noise(position.xyz * 1.0f + noise_transform.xyz) * 0.250f;
	
	float4 texcoord = IN.texcoord_0;
	texcoord.xy = texcoord.xy * base_transform.xy + base_transform.zw;
	
	normal *= s_polygon_front;
	
	#include <core/shaders/default/mesh/vertex_base.h>
	
	return OUT;
}

#endif
