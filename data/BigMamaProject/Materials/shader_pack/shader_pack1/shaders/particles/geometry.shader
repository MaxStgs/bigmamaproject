/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


#include <core/shaders/default/common/geometry_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

layout(points) in;
layout(triangle_strip,max_vertices = 4) out;

in float2 s_geom_texcoord_0[];

/*
 */
void main() {
	
	float2 parameters = s_geom_texcoord_0[0];
	
	float s = sin(parameters.y) * parameters.x;
	float c = cos(parameters.y) * parameters.x;
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(-c,-s,0.0f,0.0f));
	s_texcoord_0.xy = float2(0.0f,0.0f);
	EmitVertex();
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(s,-c,0.0f,0.0f));
	s_texcoord_0.xy = float2(1.0f,0.0f);
	EmitVertex();
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(-s,c,0.0f,0.0f));
	s_texcoord_0.xy = float2(0.0f,1.0f);
	EmitVertex();
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(c,s,0.0f,0.0f));
	s_texcoord_0.xy = float2(1.0f,1.0f);
	EmitVertex();
	
	EndPrimitive();
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

struct GEOMETRY_IN {
	float4 position : SV_POSITION;
	float2 texcoord_0 : TEXCOORD0;
};

struct GEOMETRY_OUT {
	float4 position : SV_POSITION;
	float2 texcoord_0 : TEXCOORD0;
};

/*
 */
[maxvertexcount(4)]
void main(point GEOMETRY_IN IN[1],inout TriangleStream<GEOMETRY_OUT> stream) {
	
	GEOMETRY_OUT OUT;
	
	float2 parameters = IN[0].texcoord_0;
	
	float s = sin(parameters.y) * parameters.x;
	float c = cos(parameters.y) * parameters.x;
	
	OUT.position = getPosition(IN[0].position + float4(-c,-s,0.0f,0.0f));
	OUT.texcoord_0 = float2(0.0f,0.0f);
	stream.Append(OUT);
	
	OUT.position = getPosition(IN[0].position + float4(s,-c,0.0f,0.0f));
	OUT.texcoord_0 = float2(1.0f,0.0f);
	stream.Append(OUT);
	
	OUT.position = getPosition(IN[0].position + float4(-s,c,0.0f,0.0f));
	OUT.texcoord_0 = float2(0.0f,1.0f);
	stream.Append(OUT);
	
	OUT.position = getPosition(IN[0].position + float4(c,s,0.0f,0.0f));
	OUT.texcoord_0 = float2(1.0f,1.0f);
	stream.Append(OUT);
	
	stream.RestartStrip();
}

#endif
