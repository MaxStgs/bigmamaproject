/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


/* s_texture_0 is velocity texture
 * s_texture_1 is position texture
 */

#include <core/shaders/default/common/fragment_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

uniform sampler2D s_texture_0;
uniform sampler2D s_texture_1;

uniform float ifps;

/*
 */
void main() {
	
	half4 velocity = texture(s_texture_0,s_texcoord_0.xy);
	half4 position = texture(s_texture_1,s_texcoord_0.xy);
	
	position.xyz += velocity.xyz * ifps;
	
	position.xyz = clamp(position.xyz,half3(-128.0f,-128.0f,2.0f),half3(128.0f,128.0f,128.0f));
	
	s_frag_color = position;
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

Texture2D s_texture_0 : register(t0);
Texture2D s_texture_1 : register(t1);

struct FRAGMENT_IN {
	float4 position : SV_POSITION;
	float2 texcoord_0 : TEXCOORD0;
};

float ifps;

/*
 */
float4 main(FRAGMENT_IN IN) : SV_TARGET {
	
	half4 velocity = s_texture_0.Sample(s_sampler_0,IN.texcoord_0.xy);
	half4 position = s_texture_1.Sample(s_sampler_1,IN.texcoord_0.xy);
	
	position.xyz += velocity.xyz * ifps;
	
	position.xyz = clamp(position.xyz,half3(-128.0f,-128.0f,2.0f),half3(128.0f,128.0f,128.0f));
	
	return position;
}

#endif
