/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


#include <core/shaders/default/common/geometry_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

layout(points) in;
layout(triangle_strip,max_vertices = 4) out;

/*
 */
void main() {
	
	float r = 0.5f;
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(-r,-r,0.0f,0.0f));
	s_texcoord_0.xy = float2(0.0f,0.0f);
	EmitVertex();
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(r,-r,0.0f,0.0f));
	s_texcoord_0.xy = float2(1.0f,0.0f);
	EmitVertex();
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(-r,r,0.0f,0.0f));
	s_texcoord_0.xy = float2(0.0f,1.0f);
	EmitVertex();
	
	gl_Position = getPosition(gl_in[0].gl_Position + float4(r,r,0.0f,0.0f));
	s_texcoord_0.xy = float2(1.0f,1.0f);
	EmitVertex();
	
	EndPrimitive();
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

struct GEOMETRY_IN {
	float4 position : SV_POSITION;
};

struct GEOMETRY_OUT {
	float4 position : SV_POSITION;
	float2 texcoord_0 : TEXCOORD0;
};

/*
 */
[maxvertexcount(4)]
void main(point GEOMETRY_IN IN[1],inout TriangleStream<GEOMETRY_OUT> stream) {
	
	GEOMETRY_OUT OUT;
	
	float r = 0.5f;
	
	OUT.position = getPosition(IN[0].position + float4(-r,-r,0.0f,0.0f));
	OUT.texcoord_0 = float2(0.0f,0.0f);
	stream.Append(OUT);
	
	OUT.position = getPosition(IN[0].position + float4(r,-r,0.0f,0.0f));
	OUT.texcoord_0 = float2(1.0f,0.0f);
	stream.Append(OUT);
	
	OUT.position = getPosition(IN[0].position + float4(-r,r,0.0f,0.0f));
	OUT.texcoord_0 = float2(0.0f,1.0f);
	stream.Append(OUT);
	
	OUT.position = getPosition(IN[0].position + float4(r,r,0.0f,0.0f));
	OUT.texcoord_0 = float2(1.0f,1.0f);
	stream.Append(OUT);
	
	stream.RestartStrip();
}

#endif
