/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


#include <core/shaders/default/common/vertex_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

in float4 s_attribute_0;

uniform sampler2D s_texture_2;

uniform float4 s_instances[96];

/*
 */
void main() {
	
	float4 row_0 = s_transform[0];
	float4 row_1 = s_transform[1];
	float4 row_2 = s_transform[2];
	
	float4 position = texelFetch(s_texture_2,int2(s_attribute_0.xy),0);
	gl_Position = mul4(row_0,row_1,row_2,position);
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

Texture2D s_texture_2 : register(t2);

struct VERTEX_IN {
	float4 position : POSITION;
	uint instance : SV_INSTANCEID;
};

struct VERTEX_OUT {
	float4 position : SV_POSITION;
};

cbuffer shader_instance_parameters {
	float4 s_instances[96];
};

/*
 */
VERTEX_OUT main(VERTEX_IN IN) {
	
	VERTEX_OUT OUT;
	
	float4 row_0 = s_transform[0];
	float4 row_1 = s_transform[1];
	float4 row_2 = s_transform[2];
	
	float4 position = s_texture_2.Load(int3(IN.position.xy,0));
	OUT.position = mul4(row_0,row_1,row_2,position);
	
	return OUT;
}

#endif
