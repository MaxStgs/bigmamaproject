/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


/* s_texture_0 is color texture
 * s_texture_1 is light texture
 * s_texture_2 is normal texture
 * s_texture_3 is gradient texture
 */

#include <core/shaders/default/common/fragment_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

uniform sampler2D s_texture_0;
uniform sampler2D s_texture_1;
uniform sampler2D s_texture_2;
uniform sampler2D s_texture_3;

/*
 */
void main() {
	
	half4 normal = texture(s_texture_2,s_texcoord_0.xy);
	
	half4 light = texture(s_texture_1,s_texcoord_0.xy + normal.xy * 0.02f);
	half4 gradient = texture(s_texture_3,half2(light.x,0.5f));
	
	half4 color = texture(s_texture_0,s_texcoord_0.xy + normal.xy * 0.12f);
	color.y = texture(s_texture_0,s_texcoord_0.xy + normal.xy * 0.10f).y;
	color.z = texture(s_texture_0,s_texcoord_0.xy + normal.xy * 0.09f).z;
	
	s_frag_color = lerp(color,gradient,gradient.w);
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

Texture2D s_texture_0 : register(t0);
Texture2D s_texture_1 : register(t1);
Texture2D s_texture_2 : register(t2);
Texture2D s_texture_3 : register(t3);

struct FRAGMENT_IN {
	float4 position : SV_POSITION;
	float4 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
};

/*
 */
float4 main(FRAGMENT_IN IN) : SV_TARGET {
	
	half4 normal = s_texture_2.Sample(s_sampler_2,IN.texcoord_0.xy);
	
	half4 light = s_texture_1.Sample(s_sampler_1,IN.texcoord_0.xy + normal.xy * 0.02f);
	half4 gradient = s_texture_3.Sample(s_sampler_3,half2(light.x,0.5f));
	
	half4 color = s_texture_0.Sample(s_sampler_0,IN.texcoord_0.xy + normal.xy * 0.12f);
	color.y = s_texture_0.Sample(s_sampler_0,IN.texcoord_0.xy + normal.xy * 0.10f).y;
	color.z = s_texture_0.Sample(s_sampler_0,IN.texcoord_0.xy + normal.xy * 0.09f).z;
	
	return lerp(color,gradient,gradient.w);
}

#endif
