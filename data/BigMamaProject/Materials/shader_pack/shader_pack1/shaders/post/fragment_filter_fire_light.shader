/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


/* s_texture_0 is light texture
 * s_texture_1 is noise texture
 */

#include <core/shaders/default/common/fragment_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

uniform sampler2D s_texture_0;
uniform sampler2D s_texture_1;

uniform half4 parameters;

/*
 */
void main() {
	
	half4 light_0 = texture(s_texture_0,s_texcoord_0.zw);
	half4 light_1 = texture(s_texture_0,s_texcoord_1.xy);
	half4 light_2 = texture(s_texture_0,s_texcoord_1.zw);
	
	half4 noise = texture(s_texture_1,s_texcoord_0.xy + parameters.zw);
	
	half4 light = (light_0 + light_1 + light_2) / max(2.3f + noise.x,3.02f);
	
	if(s_texcoord_0.y < s_viewport.w) light = noise;
	
	s_frag_color = light;
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

Texture2D s_texture_0 : register(t0);
Texture2D s_texture_1 : register(t1);

struct FRAGMENT_IN {
	float4 position : SV_POSITION;
	float4 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
};

float4 parameters;

/*
 */
float4 main(FRAGMENT_IN IN) : SV_TARGET {
	
	half4 light_0 = s_texture_0.Sample(s_sampler_0,IN.texcoord_0.zw);
	half4 light_1 = s_texture_0.Sample(s_sampler_0,IN.texcoord_1.xy);
	half4 light_2 = s_texture_0.Sample(s_sampler_0,IN.texcoord_1.zw);
	
	half4 noise = s_texture_1.Sample(s_sampler_1,IN.texcoord_0.xy + parameters.xy);
	
	half4 light = (light_0 + light_1 + light_2) / max(2.3f + noise.x,3.02f);
	
	if(IN.texcoord_0.y > (s_viewport.y - 1.0f) * s_viewport.w) light = noise;
	
	return light;
}

#endif
