/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


/* s_texture_0 is height texture
 */

#include <core/shaders/default/common/fragment_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

uniform sampler2D s_texture_0;

/*
 */
void main() {
	
	half4 height_0 = texture(s_texture_0,s_texcoord_0.xy);
	half4 height_1 = texture(s_texture_0,s_texcoord_0.zw);
	half4 height_2 = texture(s_texture_0,s_texcoord_1.xy);
	
	half3 vertex_0 = half3(0.03f,0.00f,height_1.x - height_0.x);
	half3 vertex_1 = half3(0.00f,0.03f,height_2.x - height_0.x);
	
	s_frag_color = half4(normalize(cross(vertex_0,vertex_1)),1.0f);
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

Texture2D s_texture_0 : register(t0);

struct FRAGMENT_IN {
	float4 position : SV_POSITION;
	float4 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
};

/*
 */
float4 main(FRAGMENT_IN IN) : SV_TARGET {
	
	half4 height_0 = s_texture_0.Sample(s_sampler_0,IN.texcoord_0.xy);
	half4 height_1 = s_texture_0.Sample(s_sampler_0,IN.texcoord_0.zw);
	half4 height_2 = s_texture_0.Sample(s_sampler_0,IN.texcoord_1.xy);
	
	half3 vertex_0 = half3(0.03f,0.00f,height_1.x - height_0.x);
	half3 vertex_1 = half3(0.00f,0.03f,height_2.x - height_0.x);
	
	return half4(normalize(cross(vertex_0,vertex_1)),1.0f);
}

#endif
