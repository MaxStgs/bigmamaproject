/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


/* s_texture_0 is height texture
 */

#include <core/shaders/default/common/fragment_base.h>

/******************************************************************************\
*
* OpenGL
*
\******************************************************************************/

#ifdef OPENGL

uniform sampler2D s_texture_0;

uniform half4 parameters;
uniform half4 random_position;

/*
 */
void main() {
	
	half4 height = texture(s_texture_0,s_texcoord_0.xy);
	
	half4 height_0 = texture(s_texture_0,s_texcoord_1.xy);
	half4 height_1 = texture(s_texture_0,s_texcoord_1.zw);
	half4 height_2 = texture(s_texture_0,s_texcoord_2.xy);
	half4 height_3 = texture(s_texture_0,s_texcoord_2.zw);
	
	half velocity = (height_0.y + height_1.y + height_2.y + height_3.y) - height.y * 4.0f;
	
	height.x = clamp(height.x + velocity * parameters.y,-1024.0f,1024.0f) * parameters.x;
	height.y = clamp(height.y + height.x * parameters.y,-1024.0f,1024.0f) * parameters.x;
	
	half distance = length(s_texcoord_0.xy - random_position.xy);
	if(distance < 0.01f) height.y = 16.0f;
	
	s_frag_color = height;
}

/******************************************************************************\
*
* Direct3D11
*
\******************************************************************************/

#elif DIRECT3D11

Texture2D s_texture_0 : register(t0);

struct FRAGMENT_IN {
	float4 position : SV_POSITION;
	float2 texcoord_0 : TEXCOORD0;
	float4 texcoord_1 : TEXCOORD1;
	float4 texcoord_2 : TEXCOORD2;
};

float4 parameters;
float4 mouse_position;
float4 random_position;

/*
 */
float4 main(FRAGMENT_IN IN) : SV_TARGET {
	
	half4 height = s_texture_0.Sample(s_sampler_0,IN.texcoord_0);
	
	half4 height_0 = s_texture_0.Sample(s_sampler_0,IN.texcoord_1.xy);
	half4 height_1 = s_texture_0.Sample(s_sampler_0,IN.texcoord_1.zw);
	half4 height_2 = s_texture_0.Sample(s_sampler_0,IN.texcoord_2.xy);
	half4 height_3 = s_texture_0.Sample(s_sampler_0,IN.texcoord_2.zw);
	
	half velocity = (height_0.y + height_1.y + height_2.y + height_3.y) - height.y * 4.0f;
	
	height.x = clamp(height.x + velocity * parameters.y,-1024.0f,1024.0f) * parameters.x;
	height.y = clamp(height.y + height.x * parameters.y,-1024.0f,1024.0f) * parameters.x;
	
	half distance = length(IN.texcoord_0 - random_position.xy);
	if(distance < 0.01f) height.y = 16.0f;
	
	return height;
}

#endif
