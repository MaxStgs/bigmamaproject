using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using Unigine;

[Component(PropertyGuid = "678f406fdd38730231519679f158cd84f1b7fb13")]
public class RealisticDroneController : Component
{
	//all for controls
	[ShowInEditor] 
	[Parameter(Group = "Control", Tooltip = "Forward axis")]
	private Input.KEY ForwardInputKey = Input.KEY.W;

	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Backward axis")]
	private Input.KEY BackwardInputKey = Input.KEY.S;

	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Left axis")]
	private Input.KEY LeftInputKey = Input.KEY.A;

	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Right axis")]
	private Input.KEY RightInputKey = Input.KEY.D;
	
	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Right turn axis")]
	private Input.KEY RightTurnInputKey = Input.KEY.E;
	
	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Left turn axis")]
	private Input.KEY LeftTurnInputKey = Input.KEY.Q;
	
	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Up axis")]
	private Input.KEY UpInputKey = Input.KEY.R;
	
	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Down axis")]
	private Input.KEY DownInputKey = Input.KEY.F;
	
	[ShowInEditor]
	[Parameter(Group = "Control", Tooltip = "Reset")]
	private Input.KEY ResetKeyInput = Input.KEY.Y;
	

	//physics
	[ShowInEditor]
	[Parameter(Group = "Physics")]
	private float gravity = 9.81f;
	
	[ShowInEditor]
	[Parameter(Group = "Physics")]
	private bool canFloat = false;
	
	//speed
	[ShowInEditor] 
	[Parameter(Group = "Speed")]
	private float speedUp = 30f;
	
	[ShowInEditor] 
	[Parameter(Group = "Speed")]
	private float speedDown = -10f;
	
	[ShowInEditor]
	[Parameter(Group = "Speed")]
	private float speedForward = 20f;
	
	[ShowInEditor]
	[Parameter(Group = "Speed")]
	private float speedBackward = 20f;
	
	[ShowInEditor]
	[Parameter(Group = "Speed")]
	private float speedLeft = 20f;

	[ShowInEditor]
	[Parameter(Group = "Speed")]
	private float speedRight = 20f;

	[ShowInEditor]
	[Parameter(Group = "Speed")]
	private float rotationSpeed = 80f;
	
	//tilt
	[ShowInEditor]
	[Parameter(Group = "Tilt")]
	private const float tiltForward = 30f;
	
	[ShowInEditor]
	[Parameter(Group = "Tilt")]
	private const float tiltBackward = 30f;
	
	[ShowInEditor] 
	[Parameter(Group = "Tilt")]
	private const float tiltLeft = 30f;
	
	[ShowInEditor]
	[Parameter(Group = "Tilt")]
	private const float tiltRight = 30f;

	//mesh
	[ShowInEditor]
	[Parameter(Group = "Mesh")]
	private Node droneMesh = null; 
	
	//vfx
	[ShowInEditor] 
	[Parameter(Group = "VFX")]
	private bool enableVFX = false;
	
	[ShowInEditor] 
	[ParameterCondition(nameof(enableVFX), 1)] 
	[Parameter(Group = "VFX")]
	private List<Node> rotors = new List<Node>();
	
	[ShowInEditor]
	[ParameterCondition(nameof(enableVFX), 1)] [Parameter(Group = "VFX")]
	private float rotorSpeed = 100f;
	
	[ShowInEditor]
	[ParameterCondition(nameof(enableVFX), 1)] [Parameter(Group = "VFX")]
	private bool enableDust = false;
	
	[ShowInEditor]
	[ParameterCondition(nameof(enableVFX), 1)] 
	[ParameterCondition(nameof(enableDust), 1)] 
	[Parameter(Group = "VFX")]
	private ObjectParticles dustParticles = null;

	[ShowInEditor]
	[ParameterCondition(nameof(enableVFX), 1)]
	[ParameterCondition(nameof(enableDust), 1)]
	[Parameter(Group = "VFX")]
	private float maxSpawnHeight = 1f;
	
	private BodyRigid droneRigid;

	private float rotX = 0;
	private float rotY = 0;
	private float rotZ = 0;

	private bool DroneEnabled = true;

	private void Init()
	{
		//Game.Player = droneCam;
		
		droneRigid = node.ObjectBodyRigid;
		dustParticles.Enabled = false;
	}

	private void Update()
	{
		if (DroneEnabled)
		{
			Controls();
			VFX();
		}
	}

	private void UpdatePhysics()
	{
		if (DroneEnabled)
		{
			RotateXYZ();
			MoveZ();
			MoveXY();
		}
	}

	private void Controls()
	{
		if (Input.IsKeyPressed(ResetKeyInput))
		{
			Reset();
		}
	}

	private void VFX()
	{
		if (enableVFX)
		{
			//rotate rotors
			var iFps = Game.IFps;
			foreach (var rotor in rotors)
			{
				rotor.Rotate(0,0,rotorSpeed * iFps);
			}
			
			//dust
			if(enableDust)
			{
				var dronePosition = node.WorldPosition;
				var underDrone = node.WorldPosition - new vec3(0, 0, maxSpawnHeight);
				WorldIntersection intersection = new WorldIntersection();
				var x = World.GetIntersection(dronePosition, underDrone, droneRigid.PhysicalMask, intersection);
				if (x != null)
				{
					dustParticles.WorldPosition = intersection.Point;
					dustParticles.Enabled = true;
				}
				else
				{
					dustParticles.Enabled = false;
				}
			}
		}
	}
	private void MoveZ()
	{
		
		if (Input.IsKeyPressed(UpInputKey))
		{
			droneRigid.AddForce(vec3.UP * speedUp);
		}
		else if (Input.IsKeyPressed(DownInputKey))
		{
			droneRigid.AddForce(vec3.UP * speedDown);
		}
		else if (!Input.IsKeyPressed(UpInputKey) && !Input.IsKeyPressed(DownInputKey))
		{
			if (canFloat) droneRigid.AddForce(vec3.UP * gravity);
			else droneRigid.AddForce(vec3.UP * gravity/10);
		}
		
		//Rotate Z 
		var iFps = Game.IFps;
		if (Input.IsKeyPressed(RightTurnInputKey))
		{
			rotZ += rotationSpeed * iFps;
		}
		else if (Input.IsKeyPressed(LeftTurnInputKey))
		{
			rotZ -= rotationSpeed * iFps;
		}
	}

	private void MoveXY()
	{
		if (Input.IsKeyPressed(ForwardInputKey))
		{
			droneRigid.AddForce(vec3.FORWARD * speedForward * MathLib.Inverse(droneMesh.GetWorldRotation()));
			rotX = -tiltForward;
			
		}
		else if (Input.IsKeyPressed(BackwardInputKey))
		{
			droneRigid.AddForce(vec3.BACK * droneMesh.GetWorldDirection() * speedBackward* MathLib.Inverse(droneMesh.GetWorldRotation()));
			rotX = tiltBackward;
		}
		else
		{
			rotX = 0;
		}

		if (Input.IsKeyPressed(LeftInputKey))
		{
			droneRigid.AddForce(vec3.LEFT * speedLeft * MathLib.Inverse(droneMesh.GetWorldRotation()));
			rotY = -tiltLeft;
		}
		else if (Input.IsKeyPressed(RightInputKey))
		{
			droneRigid.AddForce(vec3.RIGHT * speedRight * MathLib.Inverse(droneMesh.GetWorldRotation()));
			rotY = tiltRight;
		}
		else
		{
			rotY = 0;
		}

		if (Input.IsKeyPressed(Input.KEY.Q))
		{
			Freeze();
		}
		else if (Input.IsKeyPressed(Input.KEY.E))
		{
			UnFreeze();
		}
	}

	private void RotateXYZ()
	{
		var rot = droneMesh.GetRotation();
		rotX += rot.x;
		rotY += rot.y;

		droneMesh.SetRotation(new quat(rotX,rotY, 0));
		droneMesh.Rotate(0,0,rotZ);
	}

	public void Disable()
	{
		this.Enabled = false;
		DroneEnabled = false;
	}

	public void Enable()
	{
		this.Enabled = true;
		DroneEnabled = true;
	}

	public void Freeze()
	{
		droneRigid.Immovable = true;
		
	}
	
	public void UnFreeze()
	{
		droneRigid.Immovable = false;
	}

	public void Reset()
	{
		node.SetWorldRotation(quat.ZERO);
	}

	public void Cut()
	{
		droneRigid.AddForce(vec3.ZERO);
		DroneEnabled = false;
	}

	public void Repower()
	{
		DroneEnabled = true;
	}
}