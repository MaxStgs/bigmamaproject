using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;
using Console = System.Console;

[Component(PropertyGuid = "3d3ac7b144449d5c6899990bb1491aeeff1e2575")]
public class SimpleDroneController : Component
{
    // controls
    [ShowInEditor] [Parameter(Group = "Control", Tooltip = "Forward axis")]
    private Input.KEY forwardKey = Input.KEY.W;

    [ShowInEditor] [Parameter(Group = "Control", Tooltip = "Backward axis")]
    private Input.KEY backwardKey = Input.KEY.S;

    [ShowInEditor] [Parameter(Group = "Control", Tooltip = "Left axis")]
    private Input.KEY moveLeftKey = Input.KEY.A;

    [ShowInEditor] [Parameter(Group = "Control", Tooltip = "Right axis")]
    private Input.KEY moveRightKey = Input.KEY.D;

    [ShowInEditor] [Parameter(Group = "Camera")]
    private PlayerDummy cam = null;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float physicalMass = 30.0f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float shapeRadius = 3f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float shapeHeight = 2f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float shapeOffset = 2f;

    [Parameter(Group = "Physics")]
    private float minFriction = 0.1f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float maxFriction = 1.0f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float minVelocity = 3.0f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float maxVelocity = 6.0f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float acceleration = 8.0f;

    [ShowInEditor] [Parameter(Group = "Physics")]
    private float damping = 8.0f;

    [ShowInEditor] [Parameter(Group = "Settings")]
    private bool renderColisionShape = true;

    private bool freeze = false;

    private ObjectDummy objectDummy;
    private BodyRigid rigid;
    private ShapeCapsule shape;

    private vec3 velocity;
    private vec3 position;

    public vec3 Position
    {
        get => position;
        set
        {
            position = value;
            Flush();
        }
    }

    private vec3 direction;

    public vec3 Direction
    {
        get => direction;
        set
        {
            direction = value;
            Flush();
        }
    }

    private float phiAngle;

    private vec3 lastPos;

    private vec3 _lastCursorPosition;

    private int Ground;


    enum State
    {
        Forward = 0,
        Backward,
        MoveLeft,
        MoveRight,
        NumStates
    };

    enum StateStatus
    {
        Diasbled = 0,
        Enabled,
        Begin,
        End
    };

    private int[] states = new int[(int) State.NumStates];
    private float[] times = new float[(int) State.NumStates];


    void Init()
    {
        rigid = new BodyRigid()
        {
            MaxAngularVelocity = 0.0f,
            Freezable = true,
            Enabled = true,
            Mass = physicalMass,
        };

        objectDummy = new ObjectDummy()
        {
            Body = rigid
        };

        shape = new ShapeCapsule(shapeRadius, shapeHeight)
        {
            Restitution = 0.0f,
            Continuous = false,
            Name = "PlayerShape",
            Body = rigid
        };


        position = node.WorldPosition;
        direction = new vec3(1.0f, 0.0f, 0.0f);
        phiAngle = 90.0f;

        for (int i = 0; i < (int) State.NumStates; i++)
        {
            states[i] = 0;
            times[i] = 0.0f;
        }

        UpdateTransform();
        SetCollisionRadius(shapeRadius);
        SetCollisionHeight(shapeHeight);

        minFriction = MathLib.Max(minFriction, 0.0f);
        maxFriction = MathLib.Max(maxFriction, 0.0f);
        minVelocity = MathLib.Max(minVelocity, 0.0f);
        maxVelocity = MathLib.Max(maxVelocity, 0.0f);
        acceleration = MathLib.Max(acceleration, 0.0f);
        damping = MathLib.Max(damping, 0.0f);

        Game.Player = cam;
        lastPos = objectDummy.WorldPosition;

        ControlsApp.MouseHandle = Input.MOUSE_HANDLE.USER;

        Visualizer.Enabled = true;
    }

    public void Update()
    {
        var ifps = Game.IFps;

        UpdateRigid(ifps);
        UpdateCamera();
        LookToCursor();

        //Otherwise cursor is gone after user switched in other window
        if (ControlsApp.MouseHandle != Input.MOUSE_HANDLE.USER)
        {
            ControlsApp.MouseHandle = Input.MOUSE_HANDLE.USER;
        }

        if (renderColisionShape)
        {
            rigid.RenderShapes();
        }
    }

    public void SetCollisionRadius(float radius)
    {
        if (MathLib.Compare(shape.Radius, radius) < 1)
        {
            vec3 up = vec3.UP;

            rigid.SetPreserveTransform(new mat4(MathLib.Translate(up * (radius - shape.Radius))) * rigid.Transform);
            shape.Radius = radius;
        }
    }

    public float GetCollisionRadius()
    {
        return shape.Radius;
    }

    public void SetCollisionHeight(float height)
    {
        if (MathLib.Compare(shape.Height, height) < 1)
        {
            vec3 up = vec3.UP;

            rigid.SetPreserveTransform(new mat4(MathLib.Translate(up * (height - shape.Height) * 0.5f)) *
                                       rigid.Transform);
            shape.Height = height;
        }
    }

    public float GetCollisionHeight()
    {
        return shape.Height;
    }

    public void LookAt(vec3 view)
    {
        view.z = node.WorldPosition.z;
        node.WorldLookAt(view);
    }

    public void Freeze()
    {
        freeze = true;
        // rigid.Frozen = true;
    }

    public void Unfreeze()
    {
        freeze = false;
        // rigid.Frozen = false;
    }

    private mat4 GetBodyTransform()
    {
        var up = vec3.UP;
        var center = position;
        return MathLib.SetTo(center, center + new vec3(direction - up * MathLib.Dot(direction, up)), up) *
               new mat4(MathLib.RotateX(-90.0f) * MathLib.RotateZ(90.0f));
    }

    private mat4 GetModelview()
    {
        var up = vec3.UP;
        var eye = position + new vec3(up * (shape.Height + shape.Radius));
        return MathLib.LookAt(eye, eye + new vec3(direction), up);
    }

    private void UpdateCamera()
    {
        var act = objectDummy.WorldPosition;
        cam.WorldPosition = act - lastPos + cam.WorldPosition;
        lastPos = act;
    }

    private void LookToCursor()
    {
        if (!freeze)
        {
            var (intersection, obj) = AdditionLib.GetHitUnderCursorIntersection(100.0f);
            if (obj)
            {
                _lastCursorPosition = intersection.Point;
            }
        }

        LookAt(_lastCursorPosition);
    }

    private int UpdateState(bool condition, int state, int begin, int end, float ifps)
    {
        if (condition && states[state] == (int) StateStatus.Diasbled && begin == 1)
        {
            states[state] = (int) StateStatus.Begin;
            times[state] = 0.0f;
            return (int) StateStatus.Begin;
        }

        if (!condition && (states[state] == (int) StateStatus.Enabled || states[state] == (int) StateStatus.Begin) &&
            end == 1)
        {
            states[state] = (int) StateStatus.End;
            return (int) StateStatus.End;
        }

        if ((condition && states[state] == (int) StateStatus.Begin) || states[state] == (int) StateStatus.Enabled)
        {
            states[state] = (int) StateStatus.Enabled;
            times[state] += ifps;
            return (int) StateStatus.Enabled;
        }

        if (states[state] == (int) StateStatus.End)
        {
            states[state] = (int) StateStatus.Diasbled;
            return (int) StateStatus.Diasbled;
        }

        return (int) StateStatus.Diasbled;
    }

    private void UpdateStates(int enabled, float ifps)
    {
        // input 
        if (!freeze && enabled == 1 && !Unigine.Console.Active)
        {
            if (Input.IsKeyPressed(forwardKey) && Input.IsKeyPressed(backwardKey))
            {
                UpdateState(false, (int) State.Forward, 1, 1, ifps);
                UpdateState(false, (int) State.Backward, 1, 1, ifps);
            }
            else
            {
                UpdateState(Input.IsKeyPressed(forwardKey), (int) State.Forward, 1, 1, ifps);
                UpdateState(Input.IsKeyPressed(backwardKey), (int) State.Backward, 1, 1, ifps);
            }

            if (Input.IsKeyPressed(moveLeftKey) && Input.IsKeyPressed(moveRightKey))
            {
                UpdateState(false, (int) State.MoveLeft, 1, 1, ifps);
                UpdateState(false, (int) State.MoveRight, 1, 1, ifps);
            }
            else
            {
                UpdateState(Input.IsKeyPressed(moveLeftKey), (int) State.MoveLeft, 1, 1, ifps);
                UpdateState(Input.IsKeyPressed(moveRightKey), (int) State.MoveRight, 1, 1, ifps);
            }
        }
        //disable all
        else
        {
            UpdateState(false, (int) State.Forward, 1, 1, ifps);
            UpdateState(false, (int) State.Backward, 1, 1, ifps);
            UpdateState(false, (int) State.MoveLeft, 1, 1, ifps);
            UpdateState(false, (int) State.MoveRight, 1, 1, ifps);
        }
    }

    private void UpdateRigid(float ifps)
    {
        var up = vec3.UP;

        var impulse = vec3.ZERO;

        vec3 tangent, binormal;
        Geometry.OrthoBasis(up, out tangent, out binormal);

        var x = new quat(up, -phiAngle) * binormal;
        var y = MathLib.Normalize(MathLib.Cross(up, x));
        var z = MathLib.Normalize(MathLib.Cross(x, y));

        UpdateStates(1, ifps);


        var xVelocity = MathLib.Dot(x, rigid.LinearVelocity);
        var yVelocity = MathLib.Dot(y, rigid.LinearVelocity);
        var zVelocity = MathLib.Dot(z, rigid.LinearVelocity);


        x = new quat(up, -phiAngle) * binormal;
        y = MathLib.Normalize(MathLib.Cross(up, x));
        z = MathLib.Normalize(MathLib.Cross(x, y));

        if (states[(int) State.Forward] > 0)
            impulse += x;
        if (states[(int) State.Backward] > 0)
            impulse -= x;
        if (states[(int) State.MoveLeft] > 0)
            impulse += y;
        if (states[(int) State.MoveRight] > 0)
            impulse -= y;

        impulse.Normalize();


        if (impulse.Length2 > MathLib.EPSILON)
            rigid.Frozen = false;

        impulse *= minVelocity;

        if (Ground > 0)
            rigid.LinearVelocity = x * xVelocity + y * yVelocity + z * zVelocity;

        var targetVelocity = MathLib.Length(new vec2(MathLib.Dot(x, impulse), MathLib.Dot(y, impulse)));

        velocity = rigid.LinearVelocity;
        var oldVelocity = MathLib.Length(new vec2(MathLib.Dot(x, velocity), MathLib.Dot(y, velocity)));

        rigid.AddLinearImpulse(impulse * (acceleration * ifps * shape.Mass));

        var currentVelocity =
            MathLib.Length(new vec2(MathLib.Dot(x, rigid.LinearVelocity), MathLib.Dot(y, rigid.LinearVelocity)));
        if (targetVelocity < MathLib.EPSILON || currentVelocity > targetVelocity)
        {
            var linearVelocity = z * MathLib.Dot(z, rigid.LinearVelocity);
            linearVelocity += (x * MathLib.Dot(x, rigid.LinearVelocity) + y * MathLib.Dot(y, rigid.LinearVelocity)) *
                              MathLib.Exp(-damping * ifps);
            rigid.LinearVelocity = linearVelocity;
        }

        currentVelocity =
            MathLib.Length(new vec2(MathLib.Dot(x, rigid.LinearVelocity), MathLib.Dot(y, rigid.LinearVelocity)));
        if (currentVelocity > oldVelocity)
        {
            if (currentVelocity > targetVelocity)
            {
                var linearVelocity = z * MathLib.Dot(z, rigid.LinearVelocity);
                linearVelocity +=
                    (x * MathLib.Dot(x, rigid.LinearVelocity) + y * MathLib.Dot(y, rigid.LinearVelocity)) *
                    targetVelocity / currentVelocity;
                rigid.LinearVelocity = linearVelocity;
            }
        }

        if (currentVelocity < MathLib.EPSILON)
            rigid.LinearVelocity = z * MathLib.Dot(z, rigid.LinearVelocity);

        if (targetVelocity < MathLib.EPSILON)
            shape.Friction = maxFriction;
        else
            shape.Friction = minFriction;

        Ground = 0;

        var cap0 = shape.BottomCap;
        var cap1 = shape.TopCap;
        for (int i = 0; i < rigid.NumContacts; i++)
        {
            var point = rigid.GetContactPoint(i);
            var normal = rigid.GetContactNormal(i);
            if (MathLib.Dot(normal, up) > 0.5f && MathLib.Dot(new vec3(point - cap0), up) < 0.0f)
                Ground = 1;
        }

        position = objectDummy.WorldTransform.GetColumn3(3);

        Flush();
    }

    private void UpdateTransform()
    {
        var up = vec3.UP;

        vec3 tangent, binormal;
        Geometry.OrthoBasis(up, out tangent, out binormal);

        position = node.WorldTransform.GetColumn3(3);
        direction = MathLib.Normalize(new vec3(node.WorldTransform.GetColumn3(1)));

        phiAngle = MathLib.Atan2(MathLib.Dot(direction, tangent), MathLib.Dot(direction, binormal)) * MathLib.RAD2DEG;

        objectDummy.WorldTransform = GetBodyTransform();
    }

    private void Flush()
    {
        var up = vec3.UP;
        position.z = position.z + shapeOffset;
        node.WorldTransform =
            MathLib.SetTo(position, position + new vec3(direction - up * MathLib.Dot(direction, up)), up) *
            new mat4(MathLib.RotateX(-90.0f));
    }
}