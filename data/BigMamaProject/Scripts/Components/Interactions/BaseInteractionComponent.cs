using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;
using UnigineApp.source.Classes;
using Console = System.Console;

[Component(PropertyGuid = "dfd95bbf2aadb612658e5e49535bd314ab26a363")]
public class BaseInteractionComponent : Component
{
    [ShowInEditor, Parameter(Title = "Id")]
    private readonly int _id = -1;

    private bool _used;

    public bool GetWasUsed() => _used;

    private void Init()
    {
        // write here code to be called on component initialization
    }

    private void Update()
    {
        // write here code to be called before updating each render frame
    }

    public bool Interact()
    {
        _used = true;
        
        // Message message;
        // if (_id == -1)
        // {
        //     Log.Error($"Node: {node.Name} have no ID for handle\n");
        // }
        // else
        // {
        //     message = SingletonMessages.GetMessageById(_id);
        //     foreach (var text in message.GetText())
        //     {
        //         Log.Message($"{text}\n");
        //         Console.WriteLine(text);
        //     }
        // }
        Journal.AddNote(_id);

        var components = node.GetComponents<Component>();
        foreach (var component in components)
        {
            var interactive = component as IInteractable;
            if (interactive == null)
            {
                continue;
            }

            return interactive.Interact();
        }

        return true;
    }
}