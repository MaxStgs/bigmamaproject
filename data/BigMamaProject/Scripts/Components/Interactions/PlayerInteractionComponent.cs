using System;
using Unigine;

[Component(PropertyGuid = "bef2f9b420d7598372d3b106b34d54ad6bfe2e28")]
public class PlayerInteractionComponent : Component
{
    private static Journal _journal = new Journal();
    
    [ShowInEditor, ParameterColor, Parameter(Title = "Checked color")]
    private readonly vec4 _checkedColor = new vec4(1.0f, 0.0f, 0.0f, 1.0f);

    [ShowInEditor, ParameterColor, Parameter(Title = "Unchecked color")]
    private readonly vec4 _uncheckedColor = new vec4(0.0f, 1.0f, 0.0f, 1.0f);

    [ShowInEditor, Parameter(Title = "Scan time", Tooltip = "Scan time in seconds")]
    private float _scanTime = 2.0f;

    [ShowInEditor,
     Parameter(Title = "Distance interaction", Tooltip = "Distance for interaction in Unigine unit(meters)")]
    private float _distanceInteraction = 2.0f;

    private Node _lastCheckedObject = null;

    private double _startTimeInteraction = 0.0f;
    private float _remaining_time = 0.0f;
    private bool _isInteraction = false;
    private Node _interactionObject = null;
    
    [ShowInEditor, Parameter(Title = "Is simple camera")]
    private bool _isSimpleCamera = false;

    private void Init()
    {
        // write here code to be called on component initialization
    }

    private void Update()
    {
        // write here code to be called before updating each render frame
        if (_isSimpleCamera)
        {
            CheckObjectBeforePlayer();
        }
        else
        {
            CheckObjectUnderCursor();
        }

        if (Input.IsKeyDown(Input.KEY.F))
        {
            Interact();
        }

        if (Input.IsKeyDown(Input.KEY.G))
        {
            Journal.ShowListOfNodes();
            GuiController.ToggleJournal();
        }

        HandleInteractionTimer();
    }

    private void CheckObjectUnderCursor()
    {
        var obj = AdditionLib.GetHitUnderCursor(100.0f);
        if (obj == null)
        {
            if (_lastCheckedObject == null) return;

            ResetSelectedObject();
            return;
        }

        var interactionComponent = obj.GetComponent<BaseInteractionComponent>();
        if (interactionComponent == null)
        {
            ResetSelectedObject();
            return;
        }

        if (obj == _lastCheckedObject)
        {
            return;
        }
        // Log.Message($"I see interactable: {obj.Name}\n");

        ToggleHighlighting(false);
        _lastCheckedObject = obj;
        ToggleHighlighting(true);
    }

    private void HandleInteractionTimer()
    {
        if (!_isInteraction)
        {
            return;
        }

        if (_remaining_time > 0.0)
        {
            _remaining_time -= Game.IFps;
        }
        else
        {
            PostProcessInteraction();
            _remaining_time = 0.0f;
        }
    }

    private void Interact()
    {
        if (_isInteraction)
        {
            return;
        }
        if (_lastCheckedObject == null)
        {
            return;
        }

        var interactionComponent = _lastCheckedObject.GetComponent<BaseInteractionComponent>();
        if (interactionComponent == null)
        {
            return;
        }

        var distance = node.WorldPosition - _lastCheckedObject.WorldPosition;
        if (distance.Length > _distanceInteraction)
        {
            return;
        }

        var timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0);
        _startTimeInteraction = timeSpan.TotalSeconds;

        var simpleDroneController = FindComponentInWorld<SimpleDroneController>();
        if (simpleDroneController == null)
        {
            Log.Error("PlayerInteractionComponent.Interact() simpleDroneController is not found\n");
        }
        else
        {
            simpleDroneController.Freeze();
        }

        Log.Message("Interaction started...\n");
        _remaining_time = _scanTime;
        _interactionObject = _lastCheckedObject;
        _isInteraction = true;
    }

    private void PostProcessInteraction()
    {
        var simpleDroneController = FindComponentInWorld<SimpleDroneController>();
        if (simpleDroneController == null)
        {
            Log.Error("");
        }
        else
        {
            simpleDroneController.Unfreeze();
        }
        
        Log.Message("Interaction over.\n");
        _isInteraction = false;
        if (_interactionObject == null)
        {
            Log.Error($"PlayerInteractionComponent.PostProcessInteraction() _interactionObject is null\n");
            return;
        }

        var interactionComponent = _interactionObject.GetComponent<BaseInteractionComponent>();
        if (interactionComponent == null)
        {
            Log.Error($"PlayerInteractionComponent.PostProcessInteraction() interactionComponent is null\n");
            return;
        }

        var result = interactionComponent.Interact();
        if (result)
        {
            if (_interactionObject == _lastCheckedObject)
            {
                ToggleHighlighting(true);
            }
        }
    }


    private void CheckObjectBeforePlayer()
    {
        var obj = AdditionLib.GetObjectBeforePlayer(20.0f);
        if (obj == null)
        {
            if (_lastCheckedObject == null) return;

            ResetSelectedObject();
            return;
        }

        var interactionComponent = obj.GetComponent<BaseInteractionComponent>();
        if (interactionComponent == null)
        {
            ResetSelectedObject();
            return;
        }

        if (obj == _lastCheckedObject)
        {
            return;
        }
        // Log.Message($"I see interactable: {obj.Name}\n");

        ToggleHighlighting(false);
        _lastCheckedObject = obj;
        ToggleHighlighting(true);
    }

    private void ResetSelectedObject()
    {
        ToggleHighlighting(false);
        _lastCheckedObject = null;
    }

    private void ToggleHighlighting(bool shouldEnable)
    {
        var newColor = new vec4(0.0f);
        if (shouldEnable)
        {
            var interactionComponent = _lastCheckedObject.GetComponent<BaseInteractionComponent>();
            newColor = interactionComponent.GetWasUsed() ? _checkedColor : _uncheckedColor;
        }

        var objectMeshStatic = ((ObjectMeshStatic) _lastCheckedObject);
        if (objectMeshStatic == null)
        {
            return;
        }

        var numSurfaces = objectMeshStatic.NumSurfaces;
        for (var j = 0; j < numSurfaces; j++)
        {
            if (shouldEnable)
            {
                objectMeshStatic.SetMaterialState("auxiliary", 1, j);
                objectMeshStatic.SetMaterialParameterFloat4("auxiliary_color", newColor, j);
            }
            else
            {
                objectMeshStatic.SetMaterialState("auxiliary", 0, j);
            }
        }
        

        return;

        // This code waiting better time... :)
        //
        // var numChild = _lastCheckedObject.NumChildren;
        // if (numChild == 0)
        // {
        // 	return;
        // }
        //
        // for (var i = 0; i < numChild; i++)
        // {
        // 	var objectMeshStatic = ((ObjectMeshStatic) _lastCheckedObject);
        // 	if (objectMeshStatic == null)
        // 	{
        // 		continue;
        // 	}
        //
        // 	var numSurfaces = objectMeshStatic.NumSurfaces;
        // 	for (var j = 0; j < numSurfaces; j++)
        // 	{
        // 		if (shouldEnable)
        // 		{
        // 			objectMeshStatic.SetMaterialState("auxiliary", 1, j);
        // 			objectMeshStatic.SetMaterialParameterFloat4("auxiliary_color", ColorLib.Red, j);
        // 		}
        // 		else
        // 		{
        // 			objectMeshStatic.SetMaterialState("auxiliary", 0, j);
        // 		}
        // 	}
        // }
    }
}