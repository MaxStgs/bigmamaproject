using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;
using Object = System.Object;

[Component(PropertyGuid = "5060b5a596d4eac0a7875f1ff83c21f21da518a9")]
public class LevelComponent : Component
{
	[ShowInEditor]
	[Parameter(Group = "Door")]
	private Node leftElevatorDoor;
	
	[ShowInEditor]
	[Parameter(Group = "Door")]
	private Node rightElevatorDoor;
	
	[ShowInEditor]
	[Parameter(Group = "Door")]
	private float doorSpeed = 10f;
	
	[ShowInEditor]
	[Parameter(Group = "Door")]
	private float doorTime = 5f;
	
	[ShowInEditor]
	[Parameter(Group = "Level")]
	private Node dronePositionElevator;

	[ShowInEditor]
	[Parameter(Group = "Player")]
	private Node nextLevelNode;

	[ShowInEditor]
	[Parameter(Group = "Player")]
	private float moveSpeed = 10f;
	
	[ShowInEditor]
	[Parameter(Group = "Trigger")]
	private PhysicalTrigger openElevatorTrigger;
	
	[ShowInEditor]
	[Parameter(Group = "Trigger")]
	private PhysicalTrigger switchLevelTrigger;

	[ShowInEditor] 
	[Parameter(Group = "Control")]
	private Input.KEY elevator = Input.KEY.E;
	
	//this
	private bool disableInput = false;

	//for the doors
	private bool elavatorTrigger = false;
	private bool doorState = false; //open or close
	private bool doorSwitch = false; //enable or disable animation
	private float timerDoor = 0;
	
	//player
	private SimpleDroneController playerDroneController;
	private bool moveState = true; //if move in or out
	private bool moveSwitch = false; // enable or disable animation

	private void Init()
	{ 
		playerDroneController = FindComponentInWorld<SimpleDroneController>();
		openElevatorTrigger.AddEnterCallback(OnEnterElevatorTrigger);
		openElevatorTrigger.AddLeaveCallback(OnLeaveElevatorTrigger);
	}
	
	private void Update()
	{
		Check();
		openElevatorTrigger.RenderVisualizer();
		
		var iFps = Game.IFps;
		AnimateDoors(iFps);
	}

	private void UpdatePhysics()
	{
		openElevatorTrigger.UpdateContacts();
	}
	

	private void Check() //checks the input
	{
		if (!disableInput)
		{
			if (Input.IsKeyPressed(elevator) && elavatorTrigger)
			{
				doorSwitch = true;
				moveState = true;
				disableInput = true;
			}
		}
	}

	private void AnimateDoors(float iFps)
	{
		if (!doorState && doorSwitch && timerDoor < doorTime)
		{
			var dirL = leftElevatorDoor.GetWorldDirection((MathLib.AXIS.Y));
			var dirR = rightElevatorDoor.GetWorldDirection((MathLib.AXIS.Y));

			leftElevatorDoor.WorldPosition += dirL * doorSpeed * iFps;
			rightElevatorDoor.WorldPosition -= dirR * doorSpeed * iFps;
	
			timerDoor += iFps;
			if (timerDoor >= doorTime)
			{
				timerDoor = 0;
				doorState = true;
				doorSwitch = false;
			}
		}
		else if (doorState && doorSwitch && timerDoor < doorTime)
		{
			var dirL = leftElevatorDoor.GetWorldDirection((MathLib.AXIS.Y));
			var dirR = rightElevatorDoor.GetWorldDirection((MathLib.AXIS.Y));

			leftElevatorDoor.WorldPosition -= dirL * doorSpeed * iFps;
			rightElevatorDoor.WorldPosition += dirR * doorSpeed * iFps;

			timerDoor += iFps;
			if (timerDoor >= doorTime)
			{
				doorState = false;
				doorSwitch = false;
				timerDoor = 0;
			}
		}
	}
	
	private void OnEnterElevatorTrigger(Body b)
	{
		if (b.FindShape("PlayerShape") != -1)
		{
			elavatorTrigger = true;
		}
	}

	private void OnLeaveElevatorTrigger(Body b)
	{
		if (b.FindShape("PlayerShape") != -1)
		{
			elavatorTrigger = false;
		}
	}
}