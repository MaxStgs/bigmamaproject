using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "a5360a2425bbcf333781caf696ea45dd10bf8d98")]
public class MainMenuComponent : Component
{
	private void Init()
	{
		// write here code to be called on component initialization
		var mainMenu = new MainMenu();
	}
	
	private void Update()
	{
		// write here code to be called before updating each render frame
		
	}
}