using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;
using UnigineApp.source.Classes;

[Component(PropertyGuid = "51f34abea21f73053b58c8a03780e37fa0061ad2")]
public class FirstPCComponent : Component, IInteractable
{
	// [ShowInEditor, Parameter(Title = "Id")] private int _id = -1;
	
	private void Init()
	{
		// write here code to be called on component initialization
		
	}
	
	private void Update()
	{
		// write here code to be called before updating each render frame
		
	}

	public bool Interact()
	{
		// Journal.AddNote(_id);
		Log.Message("Hello from FirstPCComponent\n");
		Log.Message("Привет от всратого ASCII\n");
		return true;
	}
}