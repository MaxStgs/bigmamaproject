@echo off
chcp 65001
setlocal EnableDelayedExpansion
set app=bin\BigMamaProject_x64d.dll

start dotnet %app%  -video_app auto -video_vsync 0 -video_refresh 0 -video_mode 1 -video_resizable 1 -video_fullscreen 0 -video_debug 0 -video_gamma 1.000000 -sound_app auto -data_path "../data/"  -engine_config "../data/unigine.cfg" -extern_plugin "FbxImporter,GLTFImporter" -console_command "config_autosave 0 && world_load \"BigMamaProject\""
