﻿namespace UnigineApp.source.Classes
{
    public interface IInteractable
    {
        bool Interact();
    }
}