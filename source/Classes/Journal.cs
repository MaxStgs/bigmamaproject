﻿using System.Collections.Generic;
using Unigine;

public class Journal
{
    private static readonly List<Message> Notes = new List<Message>();

    private static float lastUsedTime;

    private static readonly float _showRate = 2.0f;
    
    public Journal()
    {
        
    }

    public static void AddNote(int id)
    {
        var message = SingletonMessages.GetMessageById(id);
        if (message == null)
        {
            Log.Error($"Journal.AddNote() message id with: {id} not found\n");
            return;
        }
        Notes.Add(message);
        var msg = $"Journal: Received new note from {message.GetOwner()}\n";
        Log.Message(msg);
        GuiController.AddMessage(msg);
        GuiController.AddToJournal(id);
    }
    
    public static void ShowListOfNodes()
    {
        if (Game.Time - lastUsedTime < _showRate)
        {
            return;
        }

        lastUsedTime = Game.Time;
        if (Notes.Count == 0)
        {
            Log.Message($"Journal: There is no messages");
            return;
        }
        Log.Message($"Journal: ");
        foreach(var message in Notes)
        {
            // Log.Message($"{message.GetText().First()}\n");
            Log.Message($"{message.GetOwner()}\n");
        }
    }
}