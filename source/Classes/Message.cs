﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Unigine;

[Serializable]
public class Message
{
    [JsonProperty("text")]
    private List<string> _text = new List<string>();

    [JsonProperty("id")]
    private int _id = -1;

    [JsonProperty("owner")] private string _owner = "None";

    [JsonProperty("useful")] private bool _useful = false;

    public IEnumerable<string> GetText() => _text;

    public int GetId() => _id;

    public bool GetIsUseful() => _useful;

    public string GetOwner() => _owner;
}