﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Unigine;
using Console = System.Console;
using File = System.IO.File;

public class SingletonMessages
{
    private static readonly List<Message> _messages = new List<Message>();
    
    private static readonly List<int> _sentMessages = new List<int>();

    [Serializable]
    private class MessagesJson
    {
        [JsonProperty("messages")]
        public Message[] Messages { get; set; }
    }

    public SingletonMessages()
    {
        const string fileName = "../data/BigMamaProject/TextContent/Messages.json";
        byte[] bytes;
        using (var fs = File.OpenRead(fileName))
        {
            bytes = new byte[fs.Length];
            fs.Read(bytes, 0, (int)fs.Length);
        }

        var stringData = Encoding.UTF8.GetString(bytes);
        // Console.WriteLine(stringData);
        var tempMessages = JsonConvert.DeserializeObject<MessagesJson>(stringData);
        if (tempMessages.Messages.Length == 0)
        {
            Log.Error("Messages is empty\n");
        }
        else
        {
            foreach (var message in tempMessages.Messages)
            {
                _messages.Add(message);
            }
        }
    }

    public static Message GetMessageById(int id)
    {
        return _messages.FirstOrDefault(messages => messages.GetId() == id);
    }

    public static List<Message> GetAllMessages() => _messages;

    public static bool SendMessage(int id)
    {
        var result = _sentMessages.Find(i => i == id);
        if (result != 0)
        {
            Log.Error($"SingletonMessages.SendMessage() id: {id} already sent\n");
            return false;
        }
        Log.Message($"SendMessage({id})\n");
        _sentMessages.Add(id);
        return true;
    }

    public static bool WasMessageSent(int id)
    {
        var result = _sentMessages.Find(i => i == id);
        return result != 0;
    }
}