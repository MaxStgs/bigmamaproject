﻿using System;
using System.Security.Cryptography.X509Certificates;
using Unigine;
using Console = System.Console;

public class JournalWidget
{
    public static JournalWidget singleton;

    private WidgetVBox _vBox, _hTop, _vContent, _hSendButton;

    private WidgetScrollBox _scrollBox, _scrollBox2;

    private WidgetButton _closeButton, _sendButton;

    private WidgetLabel _label;

    private bool _debug = false;

    private int _currentOpenedNote = 0;
    
    public JournalWidget()
    {
        singleton = this;
        
        var gui = Gui.Get();
        
        // General box
        _vBox = new WidgetVBox(gui);
        _vBox.Background = 1;
        // TODO: Change color looking as environment
        _vBox.BackgroundColor = new vec4(0.0f, 1.0f, 1.0f, 0.8f);
        _vBox.Height = (int)(gui.Height * 0.80);
        _vBox.Width = (int)(gui.Width * 0.60);
        gui.AddChild(_vBox, Gui.ALIGN_CENTER | Gui.ALIGN_OVERLAP);
        _vBox.Hidden = true;

        // Top box
        _hTop = new WidgetHBox(gui);
        _hTop.Background = 1;
        _hTop.Width = _vBox.Width;
        _hTop.Height = 50;
        _hTop.BackgroundColor = new vec4(1.0f, 0.0f, 1.0f, 0.8f); 
        _vBox.AddChild(_hTop, Gui.ALIGN_TOP | Gui.ALIGN_EXPAND);
        
        // Send button container
        _hSendButton = new WidgetHBox(gui);
        // _hSendButton.Background = 1;
        _hSendButton.Width = 200;
        _hSendButton.Height = 50;
        _hSendButton.SetPadding(15, 15, 0, 0);
        // _hSendButton.BackgroundColor = new vec4(1.0f, 0.0f, 1.0f, 0.8f); 
        _hTop.AddChild(_hSendButton, Gui.ALIGN_RIGHT);
        
        // Send button
        _sendButton = new WidgetButton(gui);
        _sendButton.Width = _hSendButton.Width;
        _sendButton.Height = _hSendButton.Height;
        _sendButton.Text = "Отправить информацию";
        _sendButton.FontSize = 15;
        _sendButton.AddCallback(Gui.CALLBACK_INDEX.CLICKED, OnSendButtonClicked);
        _hSendButton.AddChild(_sendButton, Gui.ALIGN_LEFT);
        
        // Close button
        _closeButton = new WidgetButton(gui);
        _closeButton.Width = 200;
        _closeButton.Height = 50;
        _closeButton.Text = "Закрыть журнал";
        _closeButton.FontSize = 15;
        _closeButton.AddCallback(Gui.CALLBACK_INDEX.CLICKED, OnCloseButtonClicked);
        _hTop.AddChild(_closeButton, Gui.ALIGN_RIGHT);

        // Content box
        _vContent = new WidgetHBox(gui);
        _vContent.Background = 1;
        _vContent.BackgroundColor = new vec4(1.0f, 0.0f, 0.5f, 0.8f);
        _vContent.Width = _vBox.Width;
        _vContent.Height = 100;
        _vBox.AddChild(_vContent, Gui.ALIGN_TOP | Gui.ALIGN_EXPAND);

        // Left list with buttons
        _scrollBox = new WidgetScrollBox(gui);
        _scrollBox.HScrollEnabled = false;
        _scrollBox.Width = _vBox.Width / 2;
        _scrollBox.Height = _vBox.Height;
        _scrollBox.Border = 0;
        _scrollBox.SetPadding(0, 20, 0, 0);
        _vContent.AddChild(_scrollBox, Gui.ALIGN_LEFT);
        
        // Right list with texts
        _scrollBox2 = new WidgetScrollBox(gui);
        _scrollBox2.HScrollEnabled = false;
        _scrollBox2.Width = _vBox.Width / 2;
        _scrollBox2.Height = _vBox.Height;
        _scrollBox2.Border = 0;
        _scrollBox2.SetPadding(20, 0, 0, 0);
        _vContent.AddChild(_scrollBox2, Gui.ALIGN_RIGHT);

        // Text inside right list
        _label = new WidgetLabel(gui);
        _label.FontSize = 20;
        _label.FontWrap = 1;
        _label.Width = _scrollBox2.Width - 10;
        _scrollBox2.AddChild(_label, Gui.ALIGN_TOP | Gui.ALIGN_LEFT);

        if (_debug)
        {
            for (var i = 1; i < 5; i++)
            {
                AddNote(i);
            }
            OpenNote(1);
        }
    }

    public void Toggle()
    {
        if (_vBox.Hidden)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    public void Open()
    {
        _vBox.Hidden = false;
        _sendButton.Hidden = true;
    }

    public void Close()
    {
        _vBox.Hidden = true;
    }

    public void AddNote(string text)
    {
        var vBox = new WidgetVBox(Gui.Get());
        vBox.Background = 1;
        vBox.BackgroundColor = new vec4(1.0f, 0.0f, 1.0f, 1.0f);

        var message = new WidgetLabel(Gui.Get(), text);
        message.FontSize = 40;
        message.FontWrap = 1;
        message.Width = _scrollBox.Width / 2;
        // scrollBox.AddChild(message, Gui.ALIGN_TOP);

        _scrollBox.AddChild(vBox, Gui.ALIGN_BOTTOM);
        vBox.AddChild(message, Gui.ALIGN_TOP);
    }

    private void OpenNote(int id)
    {
        if (id == 0)
        {
            _label.Text = "";
            _sendButton.Hidden = true;
        }
        var message = SingletonMessages.GetMessageById(id);
        if (message == null)
        {
            return;
        }

        _label.Text = "";
        foreach (var text in message.GetText())
        {
            _label.Text += "\n- " + text + "\n";
        }

        _sendButton.Hidden = SingletonMessages.WasMessageSent(id);
        _currentOpenedNote = id;
    }

    public void AddNote(int id)
    {
        var gui = Gui.Get();
        var message = SingletonMessages.GetMessageById(id);
        if (message == null)
        {
            Log.Error($"JournalWidget.AddNote() id: {id} message with id not found");
            return;
        }
        var button = new WidgetButton(gui);
        button.Text = message.GetOwner();
        button.Width = _scrollBox.Width;
        button.Height = 60;
        button.FontSize = 20;
        button.Data = message.GetId().ToString();
        button.AddCallback(Gui.CALLBACK_INDEX.CLICKED, OnButtonClicked);
        
        _scrollBox.AddChild(button, Gui.ALIGN_EXPAND);
    }

    private void OnButtonClicked(Widget widget)
    {
        int.TryParse(widget.Data, out var value);
        if (value == -1)
        {
            Log.Error($"JournalWidget.OnButtonClicked() widget.data: {widget.Data} but can no be parsed\n");
        }
        OpenNote(value);
    }

    private void OnCloseButtonClicked(Widget widget)
    {
        Close();
    }

    private void OnSendButtonClicked(Widget widget)
    {
        var success = SingletonMessages.SendMessage(_currentOpenedNote);
        if (success)
        {
            _sendButton.Hidden = true;
        }
        else
        {
            Log.Error($"JournalWidget.OnSendButtonClicked() sending message with id {_currentOpenedNote} was failed\n");
        }
    }
}