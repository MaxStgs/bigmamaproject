﻿// This class used for contains messages after interaction

using System.Timers;
using Unigine;

public class MessagesWidget
{
    public MessagesWidget Widget { get; private set; }

    private readonly WidgetHBox vBox;

    private readonly WidgetHBox MessagesBox;

    // private readonly WidgetGridBox gridBox;

    private readonly WidgetScrollBox scrollBox;

    private readonly bool _debug = false;

    public MessagesWidget()
    {
        Widget = this;

        var gui = Gui.Get();

        vBox = new WidgetHBox(gui);
        vBox.SetPadding(0, 0, 50, 50);
        if (_debug)
        {
            vBox.Background = 1;
            vBox.BackgroundColor = new vec4(0.0f, 1.0f, 0.0f, 0.5f);
        }

        vBox.Height = gui.Height - 100;
        gui.AddChild(vBox, Gui.ALIGN_EXPAND);

        MessagesBox = new WidgetHBox(gui);
        MessagesBox.Background = 1;
        MessagesBox.Width = 300;
        MessagesBox.Height = vBox.Height;
        // MessagesBox.BackgroundColor = new vec4(0.0f, 0.0f, 1.0f, 0.5f);
        MessagesBox.BackgroundColor = new vec4(0.0f, 0.0f, 0.0f, 0.0f);
        vBox.AddChild(MessagesBox, Gui.ALIGN_RIGHT);


        scrollBox = new WidgetScrollBox(gui, 30, 30);
        // scrollBox.BackgroundColor = new vec4(0.0f, 0.0f, 1.0f, 0.5f);
        scrollBox.Width = MessagesBox.Width;
        scrollBox.Height = MessagesBox.Height;
        scrollBox.Border = 0;
        MessagesBox.AddChild(scrollBox, Gui.ALIGN_TOP);

        if (_debug)
        {
            AddMessage("Hello");
            AddMessage("Hello2");
        }
    }

    public void AddMessage(string text)
    {
        // var box = new WidgetHBox(Gui.Get());
        // box.Height = 50;
        // box.Width = 50;
        // box.BackgroundColor = new vec4(1.0f, 0.0f, 0.0f, 0.5f);
        // MessagesBox.AddChild(box, Gui.ALIGN_TOP);

        // gridBox.AddChild(new WidgetLabel(Gui.Get(), "Item 0") { FontSize = 30 }, Gui.ALIGN_CENTER);

        var vBox = new WidgetVBox(Gui.Get());
        vBox.Background = 1;
        vBox.BackgroundColor = new vec4(0.0f, 0.0f, 1.0f, 0.5f);

        var message = new WidgetLabel(Gui.Get(), text);
        message.FontSize = 15;
        message.FontWrap = 1;
        message.Width = (int)(MessagesBox.Width * 0.75);
        // scrollBox.AddChild(message, Gui.ALIGN_TOP);

        scrollBox.AddChild(vBox, Gui.ALIGN_BOTTOM);
        vBox.AddChild(message, Gui.ALIGN_TOP);
        
        var timer = new Timer {Interval = 3000.0, Enabled = true};
        timer.Elapsed += (sender, args) =>
        {
            scrollBox.RemoveChild(vBox);
        };
    }
}