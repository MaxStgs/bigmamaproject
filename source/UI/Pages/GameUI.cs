﻿using Unigine;

public class GameUI : BaseGui
{

    private WidgetLabel healthText;
    
    public override string GetPageName()
    {
        return "GameUI";
    }

    public override string GetPageLink()
    {
        return "Layouts/GameUI.ui";
    }

    public GameUI()
    {
        var ui = GetUserInterface();
        var healthTextId = ui.FindWidget("HealthText");
        if (healthTextId != -1)
        {
            var widget = ui.GetWidget(healthTextId);
            healthText = (WidgetLabel) widget;
            if (healthText == null)
            {
                Log.Message("GameUI() can not cast WidgetLabel\n");
            }
        }
    }

    public override void Update()
    {
        UpdateHealth();
    }

    private void UpdateHealth()
    {
        var healthComponent = Game.Player.RootNode.GetComponentInChildren<HealthComponent>();
        if (healthComponent == null || healthText == null)
        {
            return;
        }
        
        healthText.Text = ((int)healthComponent.GetHealth()).ToString();
    }
}